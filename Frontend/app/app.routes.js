var app = angular.module('WalletApp', ['ngRoute']);
app.config(function ($routeProvider, $httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $routeProvider.
        when("/", {
            templateUrl: "./app/shared/home.html"
        })
        .when("/login", {
            templateUrl: "./app/components/login.html"
        })
        .when("/signup", {
            templateUrl: "./app/components/signup.html"
        })
        .when("/user/:id/options", {
            templateUrl: "./app/components/options.html"
        })

        .when("/user/:id/account", {
            templateUrl: "./app/components/account.html"
        })

        .when("/user/:id/notification", {
            templateUrl: "./app/components/notifications.html"
        })

        .when("/user/:id/bank", {
            templateUrl: "./app/components/bank.html"
        })
        .when("/user/:id/bank/:bankName", {
            templateUrl: "./app/components/bankupdate.html"
        })

        .when("/user/:id/friend", {
            templateUrl: "./app/components/friend.html"
        })
        .when("/user/:id/friend/:mobileNumber", {
            templateUrl: "./app/components/friendupdate.html"
        })
        .when("/user/:id/recharge/:choice/wallet", {
            templateUrl: "./app/components/add.html"
        })
        .when("/user/:id/pay", {
            templateUrl: "./app/components/pay.html"
        })

        .when("/user/:id/recharge/:choice", {
            templateUrl: "./app/components/recharge.html"
        })
        .when("/user/:id/referral", {
            templateUrl: "./app/components/referral.html"
        })
        .when("/user/:id/send/:mobileNumber", {
            templateUrl: "./app/components/transfer.html"
        })
        .when("/user/:id/wallet", {
            templateUrl: "./app/components/wallet.html"
        })
        .when("/logout", {
            templateUrl: "./app/components/logout.html"
        })

});

app.controller("MainController", function ($scope, $http, $location, $rootScope) {
    $scope.page = {};
});
app.controller("signupController", function ($scope, $http, $location, $rootScope, $routeParams) {
    $scope.signup = function () {
        if($scope.signup.firstName==undefined ||
            $scope.signup.mobileNumber==undefined ||
            $scope.signup.password==undefined|| $scope.signup.emailId==undefined)
            $.notify("Fill all Details ","error")
        else{
        var req = {
            method: 'POST',
            url: 'http://localhost:3000/signup',
            data: {
                firstName: $scope.signup.firstName,
                lastName: $scope.signup.lastName,
                mobileNumber: $scope.signup.mobileNumber,
                password: $scope.signup.password,
                emailId: $scope.signup.emailId,
                referralcode: $scope.signup.referralcode
            }
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            if (response.data == 'success') {
                //$.notify("Try Login","success");
                $.notify("Try Login", "success");
                $location.path('/login')

            }
            // $scope.signup.message = "SUCCESSFULLY SIGNED UP!!!GO BACK TO HOME PAGE AND CHECK LOGIN"

        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })
        }
    }
    $scope.back = function () {
        $location.path("/")

    }
});

app.controller("loginController", function ($scope, $http, $location, $rootScope, $routeParams) {
    $scope.login = function () {
        var req = {
            method: 'POST',
            url: 'http://localhost:3000/login',
            data: {
                mobileNumber: $scope.login.mobileNumber,
                password: $scope.login.password,
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            if ($scope.response == "invalid") {
                $scope.login.message = "INVALID DETAILS!!!"
                //$.notify("Invalid Details","error");
                $.notify("Invalid Details ", "error");
            }
            else
            { $location.path("/user/" + $scope.response + "/options") }
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                //$.notify("Bad Request ","error");
                $.notify("Bad Request ", "error");
            }
        })

    }
    $scope.back = function () {
        //console.log($routeParams.param);
        $location.path("/signup")

    }
});

app.controller("optController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    $http.get('http://localhost:3000/user/' + id + '/notifications').then(function success(response) {
        $scope.count = (response.data)
        $.notify("" + response.data + " Unread Notifications!", "success");
    },
        function error() {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                //$.notify("Bad Request ","error");
                $.notify("Bad Request ", "error");
            }
        });
    $scope.opt = {}
    $scope.opt.wallet = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/wallet")

    }
    $scope.opt.account = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/account")

    }

    $scope.opt.notify = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/notification")
    }

    $scope.opt.bank = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/bank")

    }
    $scope.opt.referral = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/referral")

    }
    $scope.opt.friend = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/friend")

    }
    $scope.opt.pay = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/pay")

    }
    $scope.opt.logout = function () {
        //console.log($routeParams.param);
        $location.path("/logout")

    }
});


app.controller("walletController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/wallet'
    }

    $http(req).then(function successCallback(response) {

        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        }
        var sortAscending = function (a, b) {
            var aDate = new Date(a["Tdate"]);
            var bDate = new Date(b["Tdate"]);
            return aDate.getTime() - bDate.getTime();
        }
        $scope.response = response.data;
        $scope.balance = $scope.response[0].balance
        $scope.star = $scope.response[0].star
        $scope.id = $scope.response[0].userId
        $scope.message = $scope.response[1].sort(sortAscending);
        // $scope.message=$scope.response[1];
        $scope.sortType = 'Tdate'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.searchtable = '';     // set the default search/filter term
    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })

    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/options")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }


})

app.controller("accountController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/account'
    }

    $http(req).then(function successCallback(response) {
        $scope.response = response.data;
        //console.log(response.data)

        if (response.data == 'System Violation') {
            $scope.message = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        }
        if (response.data == 'invalid') {
            $scope.message = "USER NOT FOUND"
            $.notify("User NOT FOUND", "error");
        }
        $scope.fname = $scope.response.firstName
        $scope.lname = $scope.response.lastName
        $scope.email = $scope.response.emailId
        $scope.id = $scope.response.userId
        $scope.count = $scope.response.count
        $scope.code = $scope.response.referralcode
        $scope.mobile = $scope.response.mobileNumber


        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = response.status;
                console.log(response.status);
            });
    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })

    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/options")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }
    $scope.update = function () {
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/account',
            data: {
                lastName: $scope.lname,
                firstName: $scope.fname,
                emailId: $scope.email
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            if ($scope.response == "success") {
                $scope.message = "Updated Successfuly!!!"
                $.notify("Updated Successfuly!!! ", "success");
            }
            if (response.data == 'System Violation') {
                $scope.message = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })

    }


})





app.controller("logoutController", function ($scope, $http, $location, $rootScope, $routeParams) {
    //var id = $routeParams.id;
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/logout'
    }

    $http(req).then(function successCallback(response) {
        console.log(response.data)
        if (response.data == 'success') {
            $.notify("LOGGED OUT SUCCESSFULLY!!", "success");
            $location.path('/')
        }
    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })

    $scope.back = function () {
        //console.log(id);
        $location.path("/")

    }


})




app.controller("referralController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/referral'
    }

    $http(req).then(function successCallback(response) {
        $scope.response = response.data;
        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        } else {
            $scope.count = response.data;
            if ($scope.count < 5)
                $scope.diff = (5 - $scope.count);
            else
                $scope.diff = 0;
        }

        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = response.status;
                console.log(response.status);
            });


    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })
    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/options")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }
     $scope. invitation= function () {
  
         if($scope.mobile>7000000000 && $scope.mobile<9999999999)
        window.alert("Invitation sent successfully")
        else
        $.notify("Invalid MobileNumber!!","error")   
         
    }


})



app.controller("bankController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/bank'
    }

    $http(req).then(function successCallback(response) {
        $scope.response = response.data;

        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        } else {
            $scope.banks = response.data
            $scope.id = id;
        }

        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = response.status;
                console.log(response.status);
            });

    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })
    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/options")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }

    $scope.add = function () {
        if($scope.bname==undefined|| $scope.card==undefined || $scope.acc==undefined)
            $.notify("Invalid Bank Details","error");
        else{
        var req = {
            method: 'POST',
            url: 'http://localhost:3000/user/' + id + '/bank',
            data: {
                bankName: $scope.bname,
                cardNo: $scope.card,
                accNo: $scope.acc
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            //console.log($scope.response)
            $scope.banks.push(req.data)
            $scope.bname = "";
            $scope.card = "";
            $scope.acc = "";
            if ($scope.response == "success") {
                $scope.msg = "Addedd Successfuly!!!"
                $.notify("Added Successfuly!!!", "success");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })
        }
    }

})



app.controller("bankupdateController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var bankName = $routeParams.bankName;
    $scope.id = id
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/bank/' + bankName
    }

    $http(req).then(function successCallback(response) {
        $scope.response = response.data;
        //console.log(response.data)
        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        } else {
            //var data=[{'bankName':response.data.bankName,'cardNo':"*********",'AccNo':"*******"}]
            $scope.banks = [];
            $scope.banks.push(response.data);
            $scope.id = id;
            //$scope.bankName = response.data.bankName;
            $scope.bname = response.data.bankName;
            //  $scope.accNo = response.data.accNo;
            $scope.acc = response.data.accNo;
            // $scope.cardNo = response.data.cardNo;
            $scope.card = response.data.cardNo;
        }
        $scope.msg = "Bank details Retrieved Successfully"
        $.notify("Bank details Retrieved Successfully", "success");

        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = response.status;
                console.log(response.status);
            });

    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })
    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/bank")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }

    $scope.update = function () {
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/bank/' + bankName,
            data: {
                bankName: $scope.bname,
                cardNo: $scope.card,
                accNo: $scope.acc
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            //console.log($scope.response)
            if ($scope.response == "success") {
                $scope.banks = []
                $scope.banks.push(req.data);
                $scope.msg = "Updated Successfuly!!! Go Back to list of banks and see changes"
                $.notify("Updated Successfuly!!! Go Back to list of banks and see changes", "success");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })

    }


    $scope.remove = function () {
        var req = {
            method: 'DELETE',
            url: 'http://localhost:3000/user/' + id + '/bank/' + bankName
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            //console.log($scope.response)
            $scope.id = "";
            $scope.bname = "";
            $scope.card = "";
            $scope.acc = "";
            $scope.banks = "";
            if ($scope.response == "success") {

                $scope.msg = "DELETED Successfuly!!! Click Go Back to  see changes"
                $.notify("Deleted Successfuly!!! Go Back to see changes", "error");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })

    }

})


//friend controller
app.controller("friendController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
console.log($scope.mobile)
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/friend'
    }

    $http(req).then(function successCallback(response) {
        $scope.response = response.data;

        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        } else {
            $scope.friends = response.data
            $scope.id = id;
        }

        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = response.status;
                console.log(response.status);
            });

    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })
    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/options")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }
    $scope.go = (x) => {
        $location.path('/user/' + id + '/friend/' + x.mobileNumber);
    }
    $scope.add = function () {
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/friend',
            data: {
                userId: $scope.id,
                mobileNumber: $scope.mobile
            }
        }
        if($scope.mobile==undefined || $scope.mobile<7000000000|| $scope.mobile>9999999999) 
            $.notify("Enter a Mobile Number")
        else{
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
             console.log($scope.mobile)
            $scope.mobile = "";
            console.log($scope.mobile)
            if ($scope.response == "success") {
                $scope.msg = "Friend Addedd Successfuly !!"
                $.notify("Friend Added Successfuly!!!", "success");
            }
            if ($scope.response == "wrong") {
                $.notify("The Given Mobile Number is not a Wallet user Try to send Invitation!!!", "error");
                //TODO// $location("/u")
            }
            if ($scope.response == "already") {
                $.notify("The Given Mobile Number is already a friend of you!!!", "error");
                //TODO// $location("/u")
            }
            if ($scope.response == "invalid") {
                $.notify("You cannot be  a friend of you!!!", "error");
                //TODO// $location("/u")
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }
            $http.get('http://localhost:3000/user/' + id + '/friend').then(function success(output) {
                $scope.friends = (output.data)

                $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                    $scope.wallet = (output.data[0].balance)
                },
                    function error() {
                        $scope.errorStatus = response.status;
                        console.log(response.status);
                    });
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });

        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })
        }
    }

})


//friendupdate controller
app.controller("friendupdateController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var mobileNumber = $routeParams.mobileNumber;
    $scope.id = id
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/friend/' + mobileNumber
    }

    $http(req).then(function successCallback(response) {
        $scope.response = response.data;
        console.log(response.data)
        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        } else {
            $scope.friends = [];
            $scope.friends.push(response.data);
            $scope.id = id;
            $scope.fname = response.data.firstName + response.data.lastName;
            $scope.mobile = response.data.mobileNumber;
            $scope.email = response.data.emailId;
        }
        $scope.msg = "Friend details Retrieved Successfully"
        $.notify("Friend details Retrieved Successfully", "success");


        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = response.status;
                console.log(response.status);
            });
    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })
    $scope.options = function () {
        console.log(id);
        $location.path("/user/" + id + "/friend")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }
    $scope.ask=()=>{
        var req = {
            method: 'POST',
            url: 'http://localhost:3000/user/' + id + '/notification',
            data: {
                amount: $scope.borrow,
                mobileNumber:parseInt(mobileNumber)
            }
        }
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            //console.log($scope.response)
            if ($scope.response == "success") {
                $.notify("Money Request sent Successfuly!!!", "success");
            }
            
           else if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }
            else {
                $.notify("Invalid Amount", "error");
            }

        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })
    }
    $scope.send = function () {
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/friend/' + mobileNumber + '/send',
            data: {
                amount: $scope.amount
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            //console.log($scope.response)
            if ($scope.response == "success") {
                $scope.msg = "Money Transferred Successfuly!!!"
                $.notify("Money Transferred Successfuly!!!", "success");
            }
            else if ($scope.response == "insufficient") {
                $scope.msg = "Insufficient Money in Wallet!!!"
            }
            else if ($scope.response == "wrong") {
                $scope.msg = "Invalid Amount!!!"
                $.notify("Invalid Amount", "error");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })

    }


    $scope.remove = function () {
        var req = {
            method: 'DELETE',
            url: 'http://localhost:3000/user/' + id + '/friend/' + mobileNumber
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            //console.log($scope.response)
            $scope.id = "";
            $scope.fname = "";
            $scope.mobile = "";
            $scope.email = "";
            $scope.friends = "";
            if ($scope.response == "success") {

                $scope.msg = "Friend Removed Successfuly!!! Click Go Back to  see changes"
                $.notify("Friend Removed Successfuly!!! Go Back to  see changes", "success");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })

    }

})


app.controller("payController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    $scope.options = function () {
        //console.log(id);
        $location.path("/user/" + id + "/options")

    }
    $scope.logout = function () {
        $location.path("/logout")

    }
    ///LOOOOOOOOOLLLL
    $scope.add = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/recharge/3/wallet")

    }
    $scope.pay = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/recharge/2")

    }
    $scope.recharge = function () {
        //console.log($routeParams.param);
        $location.path("/user/" + $routeParams.id + "/recharge/1")

    }
    $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
        $scope.wallet = (output.data[0].balance)
    },
        function error() {
            $scope.errorStatus = response.status;
            console.log(response.status);
        });

    $scope.transfer = function () {
        //console.log($routeParams.param);
        if ($scope.mobile > 9999999999 || $scope.mobile < 7000000000 || $scope.mobile == undefined)
        { $scope.msg = "Invalid Mobile Number!!!" 
            $.notify("Invalid MobileNumber!!","error")    
    }
        else if ($scope.amount < 0 || $scope.mobile == undefined) {
            $scope.msg = "Invalid Amount"
             $.notify("Invalid Amount!!","error")    
        }
        else {
            var req = {
                method: 'PUT',
                url: 'http://localhost:3000/user/' + id + '/send/' + $scope.mobile,
                data: {
                    amount: $scope.amount
                }
            }
            //console.log(req);
            $http(req).then(function successCallback(response) {
                $scope.response = response.data;
                //console.log($scope.response)
                if ($scope.response == "success") {
                    $scope.msg = "Money Transferred Successfuly!!!"
                    $.notify("Money Transferred Successfuly!!!", "success");
                }
                else if ($scope.response == "insufficient") {
                    $scope.msg = "Insufficient Money in Wallet!!!"
                    $.notify("Insufficient Money in Wallet", "error");
                }
                else if ($scope.response == "wrong") {
                    $scope.msg = "Invalid Amount!!!"
                    $.notify("Invalid Amount", "error");

                }
                if (response.data == 'System Violation') {
                    $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                    $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                    $location.path("/login")
                }

                $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                    $scope.wallet = (output.data[0].balance)
                },
                    function error() {
                        $scope.errorStatus = response.status;
                        console.log(response.status);
                    });
            }, function errorCallback(response) {
                $scope.errorStatus = response.status;
                console.log(response.status);
                if (response.status == 401) {
                    $location.path('/login')
                }
                if (response.status == 400) {
                    $.notify("Bad Request ", "error");
                }
            })
        }

    }
});

app.controller("rechargeController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var choice = $routeParams.choice;
    $scope.id = id;
    if (choice == 1)
        $scope.choice = "Recharge";
    if (choice == 2)
        $scope.choice = "Shop";
    
    

    $scope.options = function () {
        //console.log(id);
        $location.path("/user/" + id + "/pay")

    }
    $scope.logout = function () {
        $location.path("/logout")
    }
    $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
        $scope.wallet = (output.data[0].balance)
    },
        function error() {
            $scope.errorStatus = response.status;
            console.log(response.status);
        });
    $scope.pay = function () {
         if ($scope.amount < 0 || $scope.amount==undefined) {
            $scope.msg = "Invalid Amount"
             $.notify("Invalid Amount","error")  
            if (choice==1)
         if ($scope.mobile > 9999999999 || $scope.mobile < 7000000000 || $scope.mobile == undefined)
        { $scope.msg = "Invalid Mobile Number!!!" 
            $.notify("Specify Correct Mobile Number","error")    
        }
        
        if(choice==2)
             if ($scope.shop==undefined)
            $.notify("Specify Shop Name","error")  
        }
        else
        {
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/recharge/' + choice,
            data: {
                amount: $scope.amount
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            if ($scope.response == "success") {
                if (choice == 1) {
                $scope.msg = "Recharged Successfuly!!!";
                    $.notify("Recharged Successfuly!!!", "success");
                }
                if (choice == 2) {
                $scope.msg = "Paid to Shop Successfully!!!";
                    $.notify("Paid to Shop Successfuly!!! ", "success");
                }
            }
            else if ($scope.response == "insufficient") {
                $scope.msg = "Insufficient Money in Wallet!!!"
                $.notify("Insufficient Money in Wallet", "error");
            }
            else if ($scope.response == "wrong") {
                $scope.msg = "Invalid Amount!!!"
                $.notify("Invalid Amount", "error");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })
        }
    }
});

app.controller("sendController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var choice = $routeParams.choice;

    $scope.options = function () {
        //console.log(id);
        $location.path("/user/" + id + "/pay")

    }
    $scope.logout = function () {
        $location.path("/logout")
    }
    $http.get('http://localhost:3000/user/' + id + '/bank').then(function success(output) {
        $scope.banks = output.data;
        //console.log($scope.banks);
        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {

            });
    },
        function error() {

        });

    $scope.add = function () {
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/recharge/' + choice,
            data: {
                amount: $scope.amount,
                from: $scope.from
            }
        }
        //console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            if ($scope.response == "success") {
                $scope.msg = "Money Addedd Successfully!!!";
                $.notify("Money Added Successfuly!!!", "success");
            }
            else if ($scope.response == "wrong") {
                $scope.msg = "Invalid Bank!!!"
                $.notify("Invalid Bank", "error");
            }
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }

            $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
                $scope.wallet = (output.data[0].balance)
            },
                function error() {
                    $scope.errorStatus = response.status;
                    console.log(response.status);
                });
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })

    }
});


app.controller("notifyController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var id = $routeParams.id;
    var req = {
        method: 'GET',
        url: 'http://localhost:3000/user/' + id + '/notification'
    }

    fun=()=>{ $http(req).then(function successCallback(response) {
        $scope.response = response.data;

        if (response.data == 'System Violation') {
            $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
            $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
            $location.path("/login")
        } else {
            $scope.updates = response.data
            $scope.id = id;
           // console.log(response.data)
        }

        $http.get('http://localhost:3000/user/' + id + '/wallet').then(function success(output) {
            $scope.wallet = (output.data[0].balance)
        },
            function error() {
                $scope.errorStatus = output.status;
                console.log(output.status);
            });

    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $location.path('/login')
        }
        if (response.status == 400) {
            $.notify("Bad Request ", "error");
        }
    })}
    fun();
    $scope.logout = function () {
        $location.path("/logout")

    }
    $scope.options = function () {
        
        $location.path("/user/" + id + "/options")
    }
    $scope.remove=(time)=>{
       // $.notify(""+time,"info");
       var req = {
            method: 'PUT',
            url: 'http://localhost:3000/user/' + id + '/notification',
            data:{
                userId:id,
                time:time
            }
        }

        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            if (response.data == 'System Violation') {
                $scope.msg = "AUTHENTICATION ERROR /SESSION EXPIRED"
                $.notify("AUTHENTICATION ERROR /SESSION EXPIRED", "error");
                $location.path("/login")
            }
            else
           { //console.log(response.data)
            fun()}
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
            if (response.status == 401) {
                $location.path('/login')
            }
            if (response.status == 400) {
                $.notify("Bad Request ", "error");
            }
        })
    }
})

