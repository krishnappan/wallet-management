const co = require('co');
const thinky = require('thinky')()
const hash = require('password-hash')
const uuid = require('uuid')
const dbSchema = require('../Schema/dbSchema')
const boom = require('boom')
const cookie = require('hapi-auth-cookie')
const dateFormat=require('dateformat')
var r = thinky.r;
class UserService {
    constructor() {
        this.User = dbSchema.User
        this.Wallet = dbSchema.Wallet
        this.Transaction = dbSchema.Transaction
        this.Friend = dbSchema.Friend
    }
    referral(req) {
        const me = this;

        return co(function* () {

            if (req.auth.credentials.userId != req.params.userId)
                return "System Violation";
            let userId = req.params.userId;
            try {
                var data = yield me.User.filter({ referralcode: userId }).run()
                var status = yield me.Wallet.filter({ userId: userId }).run()
                if (data.length >= 5) {
                    yield r.table('user').filter({ userId: userId }).update({ count: data.length }).run();
                    if (status[0].star == false)
                        yield r.table('wallet').filter({ userId: userId }).update({
                            star: true,
                            balance: (r.expr(status[0].balance).add(500))
                        }).run();
                    //console.log(req.auth.credentials.userId);                           
                    return (data.length)
                }
                else if (data.length >= 0) {
                    yield me.User.filter({ userId: userId }).update({ count: data.length }).run();
                    yield me.Wallet.filter({ userId: userId }).update({ star: false }).run();
                    return ( data.length)
                }
                else
                    return ("invalid");
            }
            catch (error) {
            }
        })
    }
    referralUtility(userId) {
        const me=this;
        return co(function* () {


            try {
                var data = yield me.User.filter({ referralcode: userId }).run()
                var status = yield me.Wallet.filter({ userId: userId }).run()
                if (data.length >= 5) {
                    yield r.table('user').filter({ userId: userId }).update({ count: data.length }).run();
                    if (status[0].star == false)
                        yield r.table('wallet').filter({ userId: userId }).update({
                            star: true,
                            balance: (r.expr(status[0].balance).add(500))
                        }).run();
                    //console.log(req.auth.credentials.userId);                           
                    return ("\nStar   User...No. of referrals:" + data.length)
                }
                else if (data.length >= 0) {
                    yield me.User.filter({ userId: userId }).update({ count: data.length }).run();
                    yield me.Wallet.filter({ userId: userId }).update({ star: false }).run();
                    return ("\nUser...No. of referrals:" + data.length)
                }
                else
                    return ("\nUserInvalid!");
            }
            catch (error) {
            }
        })

    }
    signup(payload) {
        const me = this;
       
        var user = {
            userId: uuid.v1(),
            mobileNumber: payload.mobileNumber,
            emailId: payload.emailId,
            password: hash.generate(payload.password),
            referralcode: payload.referralcode,
            firstName: payload.firstName,
            lastName: payload.lastName,
        }
        var wallet = {
            userId: user.userId,
            star: false,
            balance: 0
        }
        var friend = {
            userId: user.userId,
            list: []
        }
        return co(function* () {
            try {

                yield me.User.save(user)
                yield me.Wallet.save(wallet)
                yield me.Friend.save(friend)
                if (user.referralcode) {
                    yield me.Friend.filter({ userId: user.referralcode }).update({ list: r.row('list').append(user.userId) });
                    yield me.Friend.filter({ userId: user.userId }).update({ list: r.row('list').append(user.referralcode) });
                }
                yield me.referralUtility(user.referralcode);
                return "success"
            }
            catch (error) {
                return "error"
            }
        })
    }

    login(req) {
        const me = this;
        let payload = req.payload;
        let message = "invalid"
        return co(function* () {
            try {

                var data = yield me.User.filter({ mobileNumber: payload.mobileNumber }).run()
                if (data.length > 0) {
                    let user = {
                        name: data[0].firstName,
                        userId: data[0].userId
                    };
                    if (hash.verify(payload.password, data[0].password)) {
                        req.cookieAuth.set(user);
                        return user.userId
                    }
                    else {
                    console.log(message)
                    return message
                }

                }
                else {
                    console.log(message)
                    return message
                }

            }
            catch (error) {
            }
        })
    }

    getUserById(req) {
        const me = this;
        let userId = req.params.userId;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                var data = yield me.User.filter({ userId: userId }).run()
                if (data[0])
                    return data[0];
                else
                    return "invalid"

            }
            catch (error) {
            }
        })
    }

    updateUserById(req) {
        const me = this;
        let payload = req.payload;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                if (payload.firstName)
                    yield me.User.filter({ userId: req.params.userId }).update({ firstName: payload.firstName }).run()
                if (payload.lastName)
                    yield me.User.filter({ userId: req.params.userId }).update({ lastName: payload.lastName }).run()
                if (payload.emailId)
                    yield me.User.filter({ userId: req.params.userId }).update({ emailId: payload.emailId }).run()
                return "success";
            }
            catch (error) {
                throw error;
            }
        })
    }

    tHistory(req) {
        const me = this;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                var wallet = yield me.Wallet.filter({ userId: req.params.userId })
                var data = yield me.Transaction.filter({ userId: req.params.userId })
                wallet.push(data);
                return wallet;
            }
            catch (error) {
            }
        })
    }




}
module.exports = UserService
/*    referral(userId){ 
        const  me = this;     
            return co(function * (){   
            try {  
            var data=yield  me.User.filter({referralcode:userId}).run()
                var status = yield me.Wallet.filter({ userId: userId }).run()  
                if ( data.length>=5){
                    yield r.table('user').filter({userId:userId} ).update({count:data.length}).run();      
                if(status[0].star == false)    
                    yield r.table('wallet').filter({ userId:userId}).update({star:true,
                                          balance:(r.expr(status[0].balance).add(500))} ).run(); 
                return ("\nStar   User...No. of referrals:"+data.length)
            }           
            else if(data.length>0){      
            yield me.User.filter({userId:userId}).update({count:data.length}).run();     
        yield me.Wallet.filter({userId:userId}).update({star:false}).run();
            return ("\nUser...No. of referrals:"+data.length)
        }
    else
            return ("\nInvalid User") ;
            }
    catch (error) {
            }  
})
}   
signup(payload){
    const me=this;
    var user={         
        userId:uuid.v1(),
        mobileNumber:  payload.mobileNumber,
        emailId:payload.emailId,
        password:hash.generate(payload.password),
        referralcode:payload.referralcode,
        firstName: payload.firstName,
        lastName: payload.lastName,
             }
    var wallet = {
    userId: user.userId,
    star: false,
    balance : 0
} 
var friend={
 userId:user.userId,
    list:[]  
}
    return co(function*(){
    try {
    yield me.User.save(user)
    yield me.Wallet.save( wallet)
    yield me.Friend.save(friend)
        if (user.referralcode)      
        {      
         yield me.Friend.filter({userId:user.referralcode}).update({list:r.row('list').append(user.userId)});
        yield me.Friend.filter({userId:user.userId}).update({list:r.row('list').append(user.referralcode)});
        }
       yield  me.referral(user.referralcode);
    return {
       msg : "User Signed Up Successfully"
    }
} 
      catch (error) {
           }
        }) 
    }
    
    login(req){  
        const
            me =this;    
        let payload=req.payload;
                     return co(function * (){       
                try {       
                var data=yield me.User.filter({mobileNumber: payload.mobileNumber}).run()
            var user={
                    name:data[0].firstName,
                    userId:data[0].userId
                }
                if(hash.verify(payload.password,data[0].password)) 
                   {    req.cookieAuth.set('session',user);
                        return ("Login Success....Welcome User:"+data[0].firstName)
   console.log(req.auth.credentials); 
                  }
            else{  
                return reply(boom.unauthorized('Bad Mobile Number or Password!'));
                     }    
                 }
                catch(error) {
                }  
                })
                }
    
    getUserById(userId){
            const me=this;
        return co(function*(){
            try {
    var data= yield me.User.filter({userId:userId}).run()
        if(data[0])
            return data[0];
        else      
            return "Invalid User!!"
             
        }      
             catch (error) {
            }        
        }) 
    }      
    updateUserById(req){
        const me=this;
    let payload = req.payload;
    return co(function*(){
            try {
        if(payload.firstName)
            yield me.User.filter({userId:req.params.userId}).update({firstName:payload.firstName}).run()
    if(payload.lastName)
    yield me.User.filter ({
        userId:req.params.userId}).update({lastName:payload.lastName}).run()
        if(payload.emailId)  
            yield me.User.filter({userId:req.params.userId}).update({emailId:payload.emailId}).run()
                return "Update Successfully";
            }      
            catch (error) {
        }  
    })

    }

tHistory(req){
                const me = this;    
                return co(function*(){  
                try {     
                    var wallet=yield me.Wallet.filter({userId:req.params.userId}) 
                      var data=yield me.Transaction.filter({userId:req.params.userId})
                    wallet.push(data);
                    return wallet;
                }
                catch(error) {
                }  
                })
                }


}
module.exports=UserService*/