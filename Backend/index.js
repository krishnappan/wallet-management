const Hapi = require('hapi')
const routes = require('./Routes/routeRegister')
const Inert = require('inert');
const Vision = require('vision');
const Pack = require('./package');
var cookie = require('hapi-auth-cookie');
const HapiSwagger = require('hapi-swagger');
const server = new Hapi.Server()

server.connection({
    port: 3000,
    routes    : {
            cors        : {
                origin   : ["*"],
                credentials : true
            }
        }
})
const options = {
    info: {
        'title': 'Test API Documentation',
        'version': Pack.version
    }
}
server.register([
    Inert,
    Vision,
    cookie,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {

        server.auth.strategy('session', 'cookie', {
            password: 'DT2AaACNZnQzvEcRBVLB6VKAT7q4c5HXa',
            cookie: 'cookie',
            isSecure: false,
            ttl: 1800000
        });
        server.auth.default({
            strategy: 'session'
        });

        server.start((err) => {
            if (err) {
                console.log(err);
            } else {

                console.log('Server running at:', server.info.uri);
            }
        })
    })

route = new routes();
route.registerRoutes(server);





