var thinky = require('thinky')();
var type = thinky.type;
var User = thinky.createModel("user", {
    userId: type.string().required(),
    mobileNumber: type.number().min(7000000000).max(9999999999).required(),
    emailId:type.string().email().required().optional().default(""),
    password:type.string().required(),
    count:type.number().default(0).optional(),
    referralcode:type.string().default("").optional(),
    firstName: type.string().required(),
    lastName: type.string().default("").optional()
}); 


var Bank=thinky.createModel("bank",{
    userId:type.string().required(),
    accNo:type.string().required(),
    cardNo:type.string().required(),
    bankName:type.string().required()
});

var Wallet=thinky.createModel("wallet",{
    userId:type.string().required(),
    star:type.boolean().default(false),
    balance:type.number().min(0)
});

var Friend=thinky.createModel("friend",{
    userId:type.string().required(),
   // list:[type.number().min(7000000000).max(9999999999)]
    list:type.array().optional().default([])
});
var Shop=thinky.createModel("shop",{
    shopId:type.number().required(),
    shopName:type.string().required(),
    description:type.string().optional()
});

var Session=thinky.createModel("session",{
    sessId:type.number().required(),
    shopId:type.number().required(),
    createdTime:type.date(),
    userId:type.string().required(),
     ttl: type.date().default(()=> {
        return this.createdTime + (3*60000);
    
    })

});

var Transaction=thinky.createModel("transaction",{
    from:type.string(),
    to:type.string(),
    amount:type.number().min(0),
    Tdate:type.date(),
    userId:type.string(),
    notation:type.string().length(1)
});

var Notification=thinky.createModel("notification",{
    userId:type.string(),
    notificationType:type.string(),
    read:type.boolean().default(false),
    msg:type.string(),
    time:type.string()

});

module.exports={
    Wallet,User,Friend,Shop,Session,Bank,Transaction,Notification
}
