const joi = require('joi');

const userSchema = joi.object({
    userId: joi.string(),
    firstName: joi.string(),
    mobileNumber: joi.number().min(7000000000).max(9999999999),
    emailId:joi.string().email().default("").optional(),
    password:joi.string(),
    count:joi.number(),
    referralcode:joi.string().default("").optional(),
    lastName: joi.string().default("").optional()

});
const userputSchema = joi.object({
  
    emailId:joi.string().email().default("").optional(),
    firstName:joi.string().default("").optional(),
    lastName: joi.string().default("").optional()

});

const loginSchema = joi.object({
    
    mobileNumber: joi.number().min(7000000000).max(9999999999).required(),
    password:joi.string().required()
    
});
const idSchema=joi.object({
    userId:joi.string().required(),
    bankName:joi.string(),
    mobileNumber:joi.number()
    
});
const choiceSchema=joi.object({
    userId:joi.string().required(),
    choice:joi.number().min(1).max(3)
});

const bankSchema = joi.object({
userId:joi.string(),
    accNo:joi.string(),
    cardNo:joi.string(),
    bankName:joi.string()
});

const walletSchema = joi.object({
userId:joi.string().required(),
    star:joi.boolean().optional(),
    balance:joi.number().min(0)
});

const friendSchema = joi.object({
userId:joi.string().required(),
    //list:[joi.number().min(7000000000).max(9999999999)]
list:joi.array().default([]).optional()
});

const shopSchema = joi.object({
shopId:joi.number().required(),
    shopName:joi.string().required(),
    description:joi.string().optional()
});

const sessionSchema = joi.object({
sessId:joi.number().required(),
    shopId:joi.number().required(),
    createdTime:joi.date(),
    userId:joi.string().required(),
     ttl: joi.date()
});

 const notificationSchema=joi.object({
    userId:joi.string(),
    notificationType:joi.string(),
    read:joi.boolean().default(false),
    msg:joi.string(),
    time:joi.string()

});

const borrowSchema=joi.object({
    amount:joi.number().min(0).required(),
    mobileNumber:joi.number().min(7000000000).max(9999999999)
});

const transactionSchema = joi.object({
    from:joi.string(),
    to:joi.string(),
    amount:joi.number().min(0).required(),
    Tdate:joi.date(),
    userId:joi.string(),
    notation:joi.string().length(1)
});

module.exports={userSchema,userputSchema,
    walletSchema,
    bankSchema,
    borrowSchema,
    transactionSchema,
    sessionSchema,
    shopSchema,
    friendSchema,
    loginSchema,
    idSchema,
    choiceSchema,
notificationSchema}
